﻿namespace Session3
{
    /// <summary>
    /// Class "Teacher" inherited from class "Person".
    /// </summary>
    class Teacher : Person
    {
        private string Subject;

        /// <summary>
        /// Explain method.
        /// </summary>
        /// <returns>An explaination</returns>
        public string Explain()
        {
            return "Explaination begins";
        }
    }
}

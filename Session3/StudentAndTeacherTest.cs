﻿using System;

namespace Session3
{
    /// <summary>
    /// Test class for Student's methods and Teacher's methods. 
    /// </summary>
    class StudentAndTeacherTest
    {
        static void Main(string[] args)
        {
            Person person1 = new Person();
            Student student1 = new Student();
            Teacher teacher1 = new Teacher();

            Console.WriteLine(person1.Say("Hello"));

            student1.SetAge(21);
            Console.WriteLine(student1.ShowAge());
            Console.WriteLine(student1.GoToClasses());

            teacher1.SetAge(30);
            Console.WriteLine(teacher1.Say("Hello"));
            Console.WriteLine(teacher1.Explain());

            Console.ReadLine();         
        }
    }
}

﻿namespace Session3
{
    /// <summary>
    /// class "Person" with some common attributes and methods.
    /// </summary>
    class Person
    {
        public string Name { get; set; }

        public string Address { get; set; }

        public int PhoneNumber { get; set; }

        protected int _age;

        /// <summary>
        /// Say method.
        /// </summary>
        /// <param name="value">A value is said.</param>
        /// <returns>Value is said.</returns>
        public string Say(string value)
        {
            return value;
        }

        /// <summary>
        /// Set age for Person.
        /// </summary>
        /// <param name="age">Age value.</param>
        public void SetAge(int age)
        {
            _age = age;
        }
    }
}

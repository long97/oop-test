﻿namespace Session3
{
    /// <summary>
    /// Class "Student" inherited from class "Person".
    /// </summary>
    class Student : Person
    {
        /// <summary>
        /// GoToClasses method.
        /// </summary>
        /// <returns>A message to notice that Student go to class.</returns>
        public string GoToClasses()
        {
            return "I'm going to class";
        }

        /// <summary>
        /// ShowAge method.
        /// </summary>
        /// <returns>My age is <age> years old.</returns>
        public string ShowAge()
        {
            return "My age is: " + _age + " years old";
        }
    }
}
